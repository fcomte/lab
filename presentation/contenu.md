<!-- .slide: class="slide" -->

<!-- .slide: class="slide" -->
# Objectifs de cette présentation

1. appréhender les concepts de l'écosystème du SI datalab
2. présenter, par l'exemple, la converture fonctionnelle actuelle du SSPCloud
3. présenter les axes de travail sur le Datalab

---

<!-- .slide: class="slide" -->
# Les 4 piliers de la transformation...

1. Pilier ingénierie logicielle : la gestion de source
2. Pilier processus de traitement : la conteneurisation
3. Pilier stockage : entrepot S3
4. Pilier expérience utilisateur : [onyxia](https://datalab.sspcloud.fr)

---

<!-- .slide: class="slide" -->
# Pilier ingénierie logicielle

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Gestion de source

- Patrimoine de code  
- Gestion de version
- Visibilité, collaboration, partage

=> Git & Gitlab / Github / Bitbucket

---

<!-- .slide: class="slide" -->
# Pilier processus de traitement

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Infrastructure mutualisée de traitement de données

Les problématiques classiques :

- le partage de ressources entre les utilisateurs
- l'adhérence à la configuration système

----

<img src=".\images\container.png" width="50%"  height="50%" class="center">

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Les avantages

- Affectation fine et efficace de ressources
- Agnostique du langage
- Indépendant de la configuration système

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Illustration dans l'eco-système BigData

Concurrence forte :

- cloud computing
- acteur de la conteneurisation

https://www.lemondeinformatique.fr/actualites/lire-cloudera-revoit-a-la-baisse-ses-previsions-sur-l-exercice-2020-75584.html

---

<!-- .slide: class="slide" -->
# Pilier stockage

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Problématique 

- l'accès à des fichiers d'input ou d'output de traitements statisques

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Le partage réseau ? 

Mauvaises propriétés :
- configuration multipartite
- ignorance de l'application sur le caractère distant de la ressource
- fonctionnalité au delà du strict nécessaire

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## S3 (Simple Storage Service)

Propulsé par AWS (Amazon Web Services)
- accessibilité (API HTTP)
- elasticité de la volumétrie

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Coté BigData

- compatible Hadoop
- moteur SQL intégré
- chiffrement coté serveur

---

<!-- .slide: class="slide" -->
# Pilier expérience utilisateur

----

<!-- .slide: class="slide" -->
## Deux volontés importantes

1. guider naturellement vers les bonnes pratiques
2. proposer une expérience non spécifique

Onyxia n'est finalement qu'une sorte de glue pour interfacer des standards de l'écosystème data

----

<!-- .slide: class="slide" -->
## Cas d'usage : 

https://datalab.sspcloud.fr

---

<!-- .slide: class="slide" -->
# Axes de travail pour l'évolution du Datalab

----

## Travail en équipe (groupe ou projet?)

1. création et gestion des droits sur le stockage
2. création et gestion des droits sur le gestionnaire de secret
3. Initialiser un projet => proposer des templates de projet (R , python)

----

## Proposer des jeux de données avec des métadonnées

enjeu de la qualité d'un lac de données / marécage de données actuel

1. recherche de jeux de données
2. pouvoir qualifier une donnée
3. visualiser les processus de production de cette donnée.
4. diffuser les processus de traitement à partir de cette donnée.

----

## Fournir un service d'automatisation de tâches

Deux problèmes :
1. Reproductibilité limitée
2. Inefficacité des traitements libre service sur l'affectation des ressources

L'objectif est de fournir au statisticien la possibilité d'automatiser ces travaux en les planifiant.

----

## Fournir un service de publication d'API adaptés aux staticiens (FAAS)

Pour aller plus loin, si on reve d'un SI directement developpé par le statiscien.

Opportunité : Fonction as a service 

1. interessant car plus pur à écrire pour le statisticien qu'une API traditionnel
2. interessant car peut être vue comme une brique élémentaire utilisable dans les workflow 

----

## Fournir un service de publication de visualisation

